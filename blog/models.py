from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.db.models import Q

class PostManager(models.Manager):
	def get_timeline(self, current_user):
		import itertools, operator
		following_list = current_user.userprofile.following.all()
		
		# We're chaining all users that we follow and our custom posts 
		q_list = itertools.chain(following_list, [current_user, ])		
		q_list = map(lambda m: Q(user = m), q_list)
		q_list = reduce(operator.or_, q_list)
		
		return super(PostManager, self).get_queryset().filter(q_list)

class Post(models.Model):
	user = models.ForeignKey(User, verbose_name = _('user'))	
	is_draft = models.BooleanField(_('is draft'), default = False, blank = True)
	is_micro = models.BooleanField(_('is micro'), default = False, blank = True, editable = False)
	
	title = models.CharField(_('title'), max_length = 250, null = True, blank = True)	
	content = models.TextField(_('content'))
	content_html = models.TextField(_('content html'), editable = False)
	
	published_at = models.DateTimeField(_('published at'), auto_now_add = True)	
	objects = PostManager()
	
	def get_hashtags(self):
		import re
		from initd.settings import HASHTAG_REGEX
		
		return re.findall(r'%s' % HASHTAG_REGEX, self.content)
	
	class Meta:
		ordering = ['-published_at', ]
	
	def save(self, *args, **kwargs):
		if len(self.content) < 160:
			self.is_micro = True
		else:
			self.is_micro = False
			
		from .templatetags.post_tags import parse_hashtags
		self.content_html = parse_hashtags(self.content)
		
		super(Post, self).save(*args, **kwargs)
		
	def __unicode__(self):
		return '%s' % self.user

###
### Signals
###########################################################################################

from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.contenttypes.models import ContentType

from users.models import Badge

@receiver(post_save, sender=Post)
def post_badges(sender, **kwargs):
	instance = kwargs['instance']
	created = kwargs['created']
	
	count = Post.objects.filter(user = instance.user, is_draft = False).count()
	
	if created:
		badges = Badge.objects.filter(content_type = ContentType.objects.get_for_model(Post))
		
		for badge in badges:
			if badge.unlock_at <= count:	
				instance.user.userprofile.badges.add(badge)
	return