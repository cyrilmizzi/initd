import json  
from django import http  
from django.template.loader import render_to_string
  
class AJAXResponseMixin(object):  
	def render_to_response(self, context):  
		"Returns a Ajax response containing 'context' as payload"
		
		return http.HttpResponse(render_to_string('blog/post_ajax.html', context))
		  
class LoginRequiredMixin(object):
	@classmethod
	def as_view(cls, **initkwargs):
		from django.contrib.auth.decorators import login_required
		
		view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
		return login_required(view)