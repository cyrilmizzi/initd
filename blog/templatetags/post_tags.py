from django import template

register = template.Library()

@register.filter
def parse_hashtags(value):
	import re
	from initd.settings import HASHTAG_REGEX
	
	return re.sub(r'%s' % HASHTAG_REGEX, lambda p: '[%s](#)' % p.group(0), value)