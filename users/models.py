from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from django.template.defaultfilters import default
from django.contrib.contenttypes.models import ContentType
from mptt.models import MPTTModel, TreeForeignKey 

class Badge(MPTTModel):
	name = models.CharField(_('name'), max_length = 255)
	description = models.TextField(_('description'))
	
	content_type = models.ForeignKey(ContentType, verbose_name = _('content type'))
	unlock_at = models.PositiveIntegerField(_('unlock at'))
	
	parent = TreeForeignKey('self', null = True, blank = True, related_name = 'children', editable = False)
	order = models.PositiveIntegerField(_('order'))
	
	def __unicode__(self):
		return '%s' % self.name
	
	class Meta:
		ordering = ['-order', ]
		
from blog.models import Post

class UserProfile(models.Model):
	'''
		Custom fields assign to each user	
	'''
	user = models.OneToOneField(User, verbose_name = _('user'))	
	following = models.ManyToManyField(User, verbose_name = _('following'), related_name = 'following_entry', null = True, blank = True)
	badges = models.ManyToManyField(Badge, null = True, blank = True)
	
	class Meta:
		verbose_name = _('User profile')
		verbose_name_plural = _('Users profiles')
		
	def __unicode__(self):
		return '%s profile' % default(self.user.get_full_name(), self.user.username)
	
	def get_followers(self):
		return User.objects.filter(userprofile__following = self.user)
	
	def get_posts(self):
		return Post.objects.filter(user = self.user)

###
### Signals
###########################################################################################

from django.dispatch import receiver
from django.db.models.signals import post_save

@receiver(post_save, sender=User)
def user_handler(sender, instance, signal, *args, **kwargs):
	'''
		When Django create a new user, we create a user profile too
	'''
	profile, created = UserProfile.objects.get_or_create(user = instance)
