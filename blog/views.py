from django.views.generic import ListView
from generic.views import AJAXResponseMixin

from .models import Post

class PostListView(ListView):
	model = Post

	def get_queryset(self):
		return self.model.objects.get_timeline(self.request.user)

class PostAJAXView(AJAXResponseMixin, ListView):
	def get(self, request, *args, **kwargs):
		from django.core import serializers
		posts = Post.objects.get_timeline(request.user).filter(pk__gte = kwargs['pk'])
		
		return self.render_to_response({'object_list': posts})