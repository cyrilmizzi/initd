from django.views.generic import DetailView, View
from django.contrib.auth.models import User
from generic.views import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect

class UserDetailView(DetailView):
	model = User
	slug_field = 'username'
	
class UserFollowView(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		user = get_object_or_404(User, pk = kwargs['pk'])
		
		request.user.userprofile.following.add(user)
		return redirect(request.META.get('HTTP_REFERER','/'))

class UserUnfollowView(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		user = get_object_or_404(User, pk = kwargs['pk'])
		
		request.user.userprofile.following.remove(user)
		return redirect(request.META.get('HTTP_REFERER','/'))