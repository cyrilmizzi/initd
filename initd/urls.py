from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from blog.views import PostListView, PostAJAXView
from users.views import UserDetailView, UserFollowView, UserUnfollowView

urlpatterns = patterns('',
    url(r'^$', PostListView.as_view(), name='post-list'),
    url(r'^ajax/posts/(?P<pk>\d+)/$', PostAJAXView.as_view(), name='post-ajax'),
    
	url(r'^user/(?P<slug>[\w.@+-]+)/$', UserDetailView.as_view(), name='user-detail'),
	url(r'^user/follow/(?P<pk>\d+)/$', UserFollowView.as_view(), name='user-follow'),
	url(r'^user/unfollow/(?P<pk>\d+)/$', UserUnfollowView.as_view(), name='user-unfollow'),

    url(r'^admin/', include(admin.site.urls)),
)
