from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin

from .models import UserProfile, Badge
from django.utils.translation import ugettext as _

class UserProfileInline(admin.StackedInline):
	model = UserProfile
	can_delete = False
	
	verbose_name = _('User profile')
	verbose_name_plural = _('User profile')
	extra = 0
	max_num = 1
	
class UserAdmin(UserAdmin):
	inlines = (UserProfileInline, )
	
class BadgeAdmin(MPTTModelAdmin, SortableModelAdmin):
	list_display = ('name', 'description', 'content_type', )
	list_filter = ('content_type', )
	
	sortable = 'order'
	
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Badge, BadgeAdmin)